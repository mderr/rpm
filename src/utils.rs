use terin::{error::Handler, show};
use std::process::Command;


fn get_string_from_output( command_stdout: Vec<u8> ) -> String {
    let mut converted_string = String::from_utf8(command_stdout).handle();
    println!("AAA {:?}", converted_string);
    converted_string.pop(); // Remove the last \n

    converted_string
}
pub fn run_command( command: &mut Command ) -> String {
    let command_output = command.output().handle(); //.expect(STRING_FAILED_TO_EXECUTE);
    let command_exit_code: i32 = command_output.status.code().unwrap();
    let command_output_string: String = get_string_from_output(command_output.stdout);

    println!("aaa {}", command_exit_code);
    // Useful debug info
    // println!("[ Command Info ]\nExit code: {}\nStderr: {}\nOutput: {}", command_exit_code, get_string_from_stdout(command_output.stderr), command_output_string );

    if command_exit_code != 0 {
        show::error( get_string_from_output(command_output.stderr) );
    }

    command_output_string
}