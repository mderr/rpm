use std::process::Command;
use users::{get_user_by_uid, get_current_uid};

use crate::utils;

const RPM_DIRECTORY: &'static str = "rpm";
const TMP_DIRECTORY: &'static str = "/tmp";
const CONFIG_DIRECTORY: &'static str = ".config";
const PACKAGE_INFO_DIRECTORY: &'static str = "package_info";

fn get_home_directory() -> String {
    let user: users::User      = get_user_by_uid( get_current_uid() ).unwrap();
    let user_name: &str        = user.name().to_str().unwrap();
    let directory_path: String = format!( "/home/{}", user_name );

    directory_path
}

pub fn create() {
    let mut command_mkdir_tmp = Command::new("mkdir");
    command_mkdir_tmp.current_dir(TMP_DIRECTORY);
    command_mkdir_tmp.args([
        "-p",
        RPM_DIRECTORY,
        &format!("{}/{}", RPM_DIRECTORY, "updates")
    ]);

    let mut command_mkdir_config = Command::new("mkdir");
    command_mkdir_config.current_dir( get_home_directory() );
    command_mkdir_config.args([
        "-p",
        &format!( "{}/{}/{}", CONFIG_DIRECTORY, RPM_DIRECTORY, PACKAGE_INFO_DIRECTORY )
    ]);

    utils::run_command(&mut command_mkdir_tmp);
    utils::run_command(&mut command_mkdir_config);
}