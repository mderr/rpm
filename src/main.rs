mod args;
mod dirs;
mod utils;

use terin;
use args::Args;

fn main() {
    let mut args = Args::new();
    terin::args::parse_into(&mut args, include_bytes!("../Cargo.toml"));

    dirs::create();
}