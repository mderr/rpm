use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Args {
    pub quiet: Option<bool>
}

impl Args {
    pub fn new() -> Self {
        Self {
            quiet: None
        }
    }
}
